package com.lentra.domain.fsl;

public class AgencyMasterDomain {

    private String agencyCode;
    private String agencyName;
    private String branchCodeServed;
    private String pinCodeServed;
    private String productsServed;
    private String bomBucketsServed;
    private String vertical;
    private String agencyPerformance;
    private String agencyCapacity;
    private String agencyStatus;
   /* private String cm;
    private String ccm;
    private String rcm;
    private String ncm;*/

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getBranchCodeServed() {
        return branchCodeServed;
    }

    public void setBranchCodeServed(String branchCodeServed) {
        this.branchCodeServed = branchCodeServed;
    }

    public String getPinCodeServed() {
        return pinCodeServed;
    }

    public void setPinCodeServed(String pinCodeServed) {
        this.pinCodeServed = pinCodeServed;
    }

    public String getProductsServed() {
        return productsServed;
    }

    public void setProductsServed(String productsServed) {
        this.productsServed = productsServed;
    }

    public String getBomBucketsServed() {
        return bomBucketsServed;
    }

    public void setBomBucketsServed(String bomBucketsServed) {
        this.bomBucketsServed = bomBucketsServed;
    }

    public String getVertical() {
        return vertical;
    }

    public void setVertical(String vertical) {
        this.vertical = vertical;
    }

    public String getAgencyPerformance() {
        return agencyPerformance;
    }

    public void setAgencyPerformance(String agencyPerformance) {
        this.agencyPerformance = agencyPerformance;
    }

    public String getAgencyCapacity() {
        return agencyCapacity;
    }

    public void setAgencyCapacity(String agencyCapacity) {
        this.agencyCapacity = agencyCapacity;
    }

    public String getAgencyStatus() {
        return agencyStatus;
    }

    public void setAgencyStatus(String agencyStatus) {
        this.agencyStatus = agencyStatus;
    }

    /*public String getCm() {
        return cm;
    }

    public void setCm(String cm) {
        this.cm = cm;
    }

    public String getCcm() {
        return ccm;
    }

    public void setCcm(String ccm) {
        this.ccm = ccm;
    }

    public String getRcm() {
        return rcm;
    }

    public void setRcm(String rcm) {
        this.rcm = rcm;
    }

    public String getNcm() {
        return ncm;
    }

    public void setNcm(String ncm) {
        this.ncm = ncm;
    }*/

    @Override
    public String toString() {
        return "AgencyMasterDomain{" +
                "agencyCode='" + agencyCode + '\'' +
                ", agencyName='" + agencyName + '\'' +
                ", branchCodeServed='" + branchCodeServed + '\'' +
                ", pinCodeServed='" + pinCodeServed + '\'' +
                ", productsServed='" + productsServed + '\'' +
                ", bomBucketsServed='" + bomBucketsServed + '\'' +
                ", vertical='" + vertical + '\'' +
                ", agencyPerformance='" + agencyPerformance + '\'' +
                ", agencyCapacity='" + agencyCapacity + '\'' +
                ", agencyStatus='" + agencyStatus + '\'' +
                '}';
    }
}
