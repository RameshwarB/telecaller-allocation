package com.lentra.domain.fsl;

import org.springframework.stereotype.Component;

@Component
public class DataDomian {

    public String loanNo;
    public String bomAllocation;
    public String dontAllocateFlag;
    public String settlementFlag;
    public String nonXInL3;
    public String rfInL3;
    public String avgReslutionDateL3;
    public String cycleDay;
    public String resolvedByHoldInLastMonth;
    public String resolutionAllocationDayAvgOfL3;
    public String resolvedByHoldInL3;
    public String representClearanceInL3;
    public String biuSelfCureDecile;
    public String linkBasedPayment;
    public String AllocatedToFieldInAllL3;
    public String holdRfToCcOrFieldInAllL3;
    public String biuRepSegment0_10;
    public String HoldLocBiuRepreLow_Cleared;
    public String HoldLocBiuRepreLow_Bounced;
    public String biuRepreLow_Cleared;
    public String biuRepreLow_Bounced;
    public String nonCCProd_Lap_Cv_Nc_Uber;
    public String ccResLastMonth;
    public String ccResolvedInL3;
    public String digitalPayments;
    public String biuRiskSegment;
    public String mob;
    public String branchCode;
    public String cityName;
    public String pinCode;
    public String broadFlag;
    public String agencyFlag;
    public String product;
    public String bomBkt1;
    public String disPositionCode;

    public String getLoanNo() {
        return loanNo;
    }

    public void setLoanNo(String loanNo) {
        this.loanNo = loanNo;
    }

    public String getBomAllocation() {
        return bomAllocation;
    }

    public void setBomAllocation(String bomAllocation) {
        this.bomAllocation = bomAllocation;
    }

    public String getRepresentClearanceInL3() {
        return representClearanceInL3;
    }

    public void setRepresentClearanceInL3(String representClearanceInL3) {
        this.representClearanceInL3 = representClearanceInL3;
    }

    public String getDontAllocateFlag() {
        return dontAllocateFlag;
    }

    public void setDontAllocateFlag(String dontAllocateFlag) {
        this.dontAllocateFlag = dontAllocateFlag;
    }

    public String getSettlementFlag() {
        return settlementFlag;
    }

    public void setSettlementFlag(String settlementFlag) {
        this.settlementFlag = settlementFlag;
    }

    public String getNonXInL3() {
        return nonXInL3;
    }

    public void setNonXInL3(String nonXInL3) {
        this.nonXInL3 = nonXInL3;
    }

    public String getRfInL3() {
        return rfInL3;
    }

    public void setRfInL3(String rfInL3) {
        this.rfInL3 = rfInL3;
    }

    public String getAvgReslutionDateL3() {
        return avgReslutionDateL3;
    }

    public void setAvgReslutionDateL3(String avgReslutionDateL3) {
        this.avgReslutionDateL3 = avgReslutionDateL3;
    }

    public String getCycleDay() {
        return cycleDay;
    }

    public void setCycleDay(String cycleDay) {
        this.cycleDay = cycleDay;
    }

    public String getResolvedByHoldInLastMonth() {
        return resolvedByHoldInLastMonth;
    }

    public void setResolvedByHoldInLastMonth(String resolvedByHoldInLastMonth) {
        this.resolvedByHoldInLastMonth = resolvedByHoldInLastMonth;
    }

    public String getResolutionAllocationDayAvgOfL3() {
        return resolutionAllocationDayAvgOfL3;
    }

    public void setResolutionAllocationDayAvgOfL3(String resolutionAllocationDayAvgOfL3) {
        this.resolutionAllocationDayAvgOfL3 = resolutionAllocationDayAvgOfL3;
    }

    public String getResolvedByHoldInL3() {
        return resolvedByHoldInL3;
    }

    public void setResolvedByHoldInL3(String resolvedByHoldInL3) {
        this.resolvedByHoldInL3 = resolvedByHoldInL3;
    }



    public String getLinkBasedPayment() {
        return linkBasedPayment;
    }

    public void setLinkBasedPayment(String linkBasedPayment) {
        this.linkBasedPayment = linkBasedPayment;
    }

    public String getAllocatedToFieldInAllL3() {
        return AllocatedToFieldInAllL3;
    }

    public void setAllocatedToFieldInAllL3(String allocatedToFieldInAllL3) {
        AllocatedToFieldInAllL3 = allocatedToFieldInAllL3;
    }

    public String getBiuSelfCureDecile() {
        return biuSelfCureDecile;
    }

    public void setBiuSelfCureDecile(String biuSelfCureDecile) {
        this.biuSelfCureDecile = biuSelfCureDecile;
    }

    public String getHoldRfToCcOrFieldInAllL3() {
        return holdRfToCcOrFieldInAllL3;
    }

    public void setHoldRfToCcOrFieldInAllL3(String holdRfToCcOrFieldInAllL3) {
        this.holdRfToCcOrFieldInAllL3 = holdRfToCcOrFieldInAllL3;
    }

    public String getBiuRepSegment0_10() {
        return biuRepSegment0_10;
    }

    public void setBiuRepSegment0_10(String biuRepSegment0_10) {
        this.biuRepSegment0_10 = biuRepSegment0_10;
    }

    public String getHoldLocBiuRepreLow_Cleared() {
        return HoldLocBiuRepreLow_Cleared;
    }

    public void setHoldLocBiuRepreLow_Cleared(String holdLocBiuRepreLow_Cleared) {
        HoldLocBiuRepreLow_Cleared = holdLocBiuRepreLow_Cleared;
    }

    public String getHoldLocBiuRepreLow_Bounced() {
        return HoldLocBiuRepreLow_Bounced;
    }

    public void setHoldLocBiuRepreLow_Bounced(String holdLocBiuRepreLow_Bounced) {
        HoldLocBiuRepreLow_Bounced = holdLocBiuRepreLow_Bounced;
    }

    public String getBiuRepreLow_Cleared() {
        return biuRepreLow_Cleared;
    }

    public void setBiuRepreLow_Cleared(String biuRepreLow_Cleared) {
        this.biuRepreLow_Cleared = biuRepreLow_Cleared;
    }

    public String getBiuRepreLow_Bounced() {
        return biuRepreLow_Bounced;
    }

    public void setBiuRepreLow_Bounced(String biuRepreLow_Bounced) {
        this.biuRepreLow_Bounced = biuRepreLow_Bounced;
    }

    public String getNonCCProd_Lap_Cv_Nc_Uber() {
        return nonCCProd_Lap_Cv_Nc_Uber;
    }

    public void setNonCCProd_Lap_Cv_Nc_Uber(String nonCCProd_Lap_Cv_Nc_Uber) {
        this.nonCCProd_Lap_Cv_Nc_Uber = nonCCProd_Lap_Cv_Nc_Uber;
    }

    public String getCcResLastMonth() {
        return ccResLastMonth;
    }

    public void setCcResLastMonth(String ccResLastMonth) {
        this.ccResLastMonth = ccResLastMonth;
    }

    public String getCcResolvedInL3() {
        return ccResolvedInL3;
    }

    public void setCcResolvedInL3(String ccResolvedInL3) {
        this.ccResolvedInL3 = ccResolvedInL3;
    }

    public String getDigitalPayments() {
        return digitalPayments;
    }

    public void setDigitalPayments(String digitalPayments) {
        this.digitalPayments = digitalPayments;
    }

    public String getBiuRiskSegment() {
        return biuRiskSegment;
    }

    public void setBiuRiskSegment(String biuRiskSegment) {
        this.biuRiskSegment = biuRiskSegment;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getBroadFlag() {
        return broadFlag;
    }

    public void setBroadFlag(String broadFlag) {
        this.broadFlag = broadFlag;
    }

    public String getAgencyFlag() {
        return agencyFlag;
    }

    public void setAgencyFlag(String agencyFlag) {
        this.agencyFlag = agencyFlag;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getBomBkt1() {
        return bomBkt1;
    }

    public void setBomBkt1(String bomBkt1) {
        this.bomBkt1 = bomBkt1;
    }

    public String getDisPositionCode() {
        return disPositionCode;
    }

    public void setDisPositionCode(String disPositionCode) {
        this.disPositionCode = disPositionCode;
    }

    @Override
    public String toString() {
        return "DataDomian{" +
                "loanNo='" + loanNo + '\'' +
                ", bomAllocation='" + bomAllocation + '\'' +
                ", dontAllocateFlag='" + dontAllocateFlag + '\'' +
                ", settlementFlag='" + settlementFlag + '\'' +
                ", nonXInL3='" + nonXInL3 + '\'' +
                ", rfInL3='" + rfInL3 + '\'' +
                ", avgReslutionDateL3='" + avgReslutionDateL3 + '\'' +
                ", cycleDay='" + cycleDay + '\'' +
                ", resolvedByHoldInLastMonth='" + resolvedByHoldInLastMonth + '\'' +
                ", resolutionAllocationDayAvgOfL3='" + resolutionAllocationDayAvgOfL3 + '\'' +
                ", resolvedByHoldInL3='" + resolvedByHoldInL3 + '\'' +
                ", representClearanceInL3='" + representClearanceInL3 + '\'' +
                ", biuSelfCureDecile='" + biuSelfCureDecile + '\'' +
                ", linkBasedPayment='" + linkBasedPayment + '\'' +
                ", AllocatedToFieldInAllL3='" + AllocatedToFieldInAllL3 + '\'' +
                ", holdRfToCcOrFieldInAllL3='" + holdRfToCcOrFieldInAllL3 + '\'' +
                ", biuRepSegment0_10='" + biuRepSegment0_10 + '\'' +
                ", HoldLocBiuRepreLow_Cleared='" + HoldLocBiuRepreLow_Cleared + '\'' +
                ", HoldLocBiuRepreLow_Bounced='" + HoldLocBiuRepreLow_Bounced + '\'' +
                ", biuRepreLow_Cleared='" + biuRepreLow_Cleared + '\'' +
                ", biuRepreLow_Bounced='" + biuRepreLow_Bounced + '\'' +
                ", nonCCProd_Lap_Cv_Nc_Uber='" + nonCCProd_Lap_Cv_Nc_Uber + '\'' +
                ", ccResLastMonth='" + ccResLastMonth + '\'' +
                ", ccResolvedInL3='" + ccResolvedInL3 + '\'' +
                ", digitalPayments='" + digitalPayments + '\'' +
                ", biuRiskSegment='" + biuRiskSegment + '\'' +
                ", mob='" + mob + '\'' +
                ", branchCode='" + branchCode + '\'' +
                ", cityName='" + cityName + '\'' +
                ", pinCode='" + pinCode + '\'' +
                ", broadFlag='" + broadFlag + '\'' +
                ", agencyFlag='" + agencyFlag + '\'' +
                ", product='" + product + '\'' +
                ", bomBkt1='" + bomBkt1 + '\'' +
                ", disPositionCode='" + disPositionCode + '\'' +
                '}';
    }
}
