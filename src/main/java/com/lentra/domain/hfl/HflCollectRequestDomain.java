package com.lentra.domain.hfl;

import com.google.gson.annotations.SerializedName;

public class HflCollectRequestDomain {

    @SerializedName("oReq")
    public HflRequest hflRequest;

    public HflRequest getHflRequest() {
        return hflRequest;
    }

    public void setHflRequest(HflRequest hflRequest) {
        this.hflRequest = hflRequest;
    }
}
