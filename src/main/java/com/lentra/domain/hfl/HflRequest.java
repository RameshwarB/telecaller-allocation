package com.lentra.domain.hfl;

import com.google.gson.annotations.SerializedName;

public class HflRequest {

    @SerializedName("oHflAcctObj")
    public AccountDomain accountDomain;

    @SerializedName("oApplication")
    public Application application;

    public AccountDomain getAccountDomain() {
        return accountDomain;
    }

    public void setAccountDomain(AccountDomain accountDomain) {
        this.accountDomain = accountDomain;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
}
