package com.lentra.domain.hfl;

import com.google.gson.annotations.SerializedName;

public class BreRequestDomain {

    @SerializedName("HEADER")
    public Header header;

    @SerializedName("HFL_COLLECT_REQUEST")
    public HflCollectRequestDomain hflCollectRequestDomain;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public HflCollectRequestDomain getHflCollectRequestDomain() {
        return hflCollectRequestDomain;
    }

    public void setHflCollectRequestDomain(HflCollectRequestDomain hflCollectRequestDomain) {
        this.hflCollectRequestDomain = hflCollectRequestDomain;
    }
}
