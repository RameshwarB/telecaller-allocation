package com.lentra.domain.hfl;

import com.google.gson.annotations.SerializedName;

public class Header {

    @SerializedName("APPLICATION-ID")
    private String applicationId;

    @SerializedName("CUSTOMER-ID")
    private String customerId;

    @SerializedName("APP-TYPE")
    private String appType;

    @SerializedName("REQUEST-TIME")
    private String requestTime;

    @SerializedName("INSTITUTION-ID")
    private String institutionId;

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }
}
