package com.lentra.domain.hfl;

public class AccountDomain {

    public String accountNo;
    public String bounceStatus;
    public String segment;
    public String band;
    public String payment;
    public String referralYesNo;
    public String referralDate;

    public String interest;
    public String charges;
    public String irregularity_OD;
    public String additionalInterest_OC;
    public String dpd;
    public String npa_wroff;
    public String finalBucketxx;
    public String emi;
    public String system;
    public String irreEmi;
    public String revised_Lob;
    public String subProductDescription;
    public String cifNo;
    public String ahf_NonAhf;
    public String status;
    public String branchCode;
    public String branch;
    public String cluster;
    public String cluster2;
    public String zone;
    public String region;
    public String contractStartDate;
    public String contractEndDate;
    public String allocationFlag;
    public String bucketingFlag;
    public String creCode;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getBounceStatus() {
        return bounceStatus;
    }

    public void setBounceStatus(String bounceStatus) {
        this.bounceStatus = bounceStatus;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getBand() {
        return band;
    }

    public void setBand(String band) {
        this.band = band;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getReferralYesNo() {
        return referralYesNo;
    }

    public void setReferralYesNo(String referralYesNo) {
        this.referralYesNo = referralYesNo;
    }

    public String getReferralDate() {
        return referralDate;
    }

    public void setReferralDate(String referralDate) {
        this.referralDate = referralDate;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getCharges() {
        return charges;
    }

    public void setCharges(String charges) {
        this.charges = charges;
    }

    public String getIrregularity_OD() {
        return irregularity_OD;
    }

    public void setIrregularity_OD(String irregularity_OD) {
        this.irregularity_OD = irregularity_OD;
    }

    public String getAdditionalInterest_OC() {
        return additionalInterest_OC;
    }

    public void setAdditionalInterest_OC(String additionalInterest_OC) {
        this.additionalInterest_OC = additionalInterest_OC;
    }

    public String getDpd() {
        return dpd;
    }

    public void setDpd(String dpd) {
        this.dpd = dpd;
    }

    public String getNpa_wroff() {
        return npa_wroff;
    }

    public void setNpa_wroff(String npa_wroff) {
        this.npa_wroff = npa_wroff;
    }

    public String getFinalBucketxx() {
        return finalBucketxx;
    }

    public void setFinalBucketxx(String finalBucketxx) {
        this.finalBucketxx = finalBucketxx;
    }

    public String getEmi() {
        return emi;
    }

    public void setEmi(String emi) {
        this.emi = emi;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getIrreEmi() {
        return irreEmi;
    }

    public void setIrreEmi(String irreEmi) {
        this.irreEmi = irreEmi;
    }

    public String getRevised_Lob() {
        return revised_Lob;
    }

    public void setRevised_Lob(String revised_Lob) {
        this.revised_Lob = revised_Lob;
    }

    public String getSubProductDescription() {
        return subProductDescription;
    }

    public void setSubProductDescription(String subProductDescription) {
        this.subProductDescription = subProductDescription;
    }

    public String getCifNo() {
        return cifNo;
    }

    public void setCifNo(String cifNo) {
        this.cifNo = cifNo;
    }

    public String getAhf_NonAhf() {
        return ahf_NonAhf;
    }

    public void setAhf_NonAhf(String ahf_NonAhf) {
        this.ahf_NonAhf = ahf_NonAhf;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getCluster2() {
        return cluster2;
    }

    public void setCluster2(String cluster2) {
        this.cluster2 = cluster2;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getContractStartDate() {
        return contractStartDate;
    }

    public void setContractStartDate(String contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public String getContractEndDate() {
        return contractEndDate;
    }

    public void setContractEndDate(String contractEndDate) {
        this.contractEndDate = contractEndDate;
    }

    public String getAllocationFlag() {
        return allocationFlag;
    }

    public void setAllocationFlag(String allocationFlag) {
        this.allocationFlag = allocationFlag;
    }

    public String getCreCode() {
        return creCode;
    }

    public void setCreCode(String creCode) {
        this.creCode = creCode;
    }

    public String getBucketingFlag() {
        return bucketingFlag;
    }

    public void setBucketingFlag(String bucketingFlag) {
        this.bucketingFlag = bucketingFlag;
    }

    @Override
    public String toString() {
        return "AccountDomain{" +
                "accountNo='" + accountNo + '\'' +
                ", bounceStatus='" + bounceStatus + '\'' +
                ", segment='" + segment + '\'' +
                ", band='" + band + '\'' +
                ", payment='" + payment + '\'' +
                ", referralYesNo='" + referralYesNo + '\'' +
                ", referralDate='" + referralDate + '\'' +
                ", interest='" + interest + '\'' +
                ", charges='" + charges + '\'' +
                ", irregularity_OD='" + irregularity_OD + '\'' +
                ", additionalInterest_OC='" + additionalInterest_OC + '\'' +
                ", dpd='" + dpd + '\'' +
                ", npa_wroff='" + npa_wroff + '\'' +
                ", finalBucketxx='" + finalBucketxx + '\'' +
                ", emi='" + emi + '\'' +
                ", system='" + system + '\'' +
                ", irreEmi='" + irreEmi + '\'' +
                ", revised_Lob='" + revised_Lob + '\'' +
                ", subProductDescription='" + subProductDescription + '\'' +
                ", cifNo='" + cifNo + '\'' +
                ", ahf_NonAhf='" + ahf_NonAhf + '\'' +
                ", status='" + status + '\'' +
                ", branchCode='" + branchCode + '\'' +
                ", branch='" + branch + '\'' +
                ", cluster='" + cluster + '\'' +
                ", cluster2='" + cluster2 + '\'' +
                ", zone='" + zone + '\'' +
                ", region='" + region + '\'' +
                ", contractStartDate='" + contractStartDate + '\'' +
                ", contractEndDate='" + contractEndDate + '\'' +
                ", allocationFlag='" + allocationFlag + '\'' +
                ", bucketingFlag='" + bucketingFlag + '\'' +
                ", creCode='" + creCode + '\'' +
                '}';
    }
}
