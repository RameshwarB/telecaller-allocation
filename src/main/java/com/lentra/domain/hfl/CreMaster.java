package com.lentra.domain.hfl;

public class CreMaster {

    /*public String region;
    public String state;
    public String tlNameAndNumber;*/
    public String creNameAndNumber;
    public String creArea;
    public String city;
    public String allocationInPercentage;

   /* public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTlNameAndNumber() {
        return tlNameAndNumber;
    }

    public void setTlNameAndNumber(String tlNameAndNumber) {
        this.tlNameAndNumber = tlNameAndNumber;
    }*/

    public String getCreNameAndNumber() {
        return creNameAndNumber;
    }

    public void setCreNameAndNumber(String creNameAndNumber) {
        this.creNameAndNumber = creNameAndNumber;
    }

    public String getCreArea() {
        return creArea;
    }

    public void setCreArea(String creArea) {
        this.creArea = creArea;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAllocationInPercentage() {
        return allocationInPercentage;
    }

    public void setAllocationInPercentage(String allocationInPercentage) {
        this.allocationInPercentage = allocationInPercentage;
    }

    @Override
    public String toString() {
        return "CreMaster{" +
                "creNameAndNumber='" + creNameAndNumber + '\'' +
                ", creArea='" + creArea + '\'' +
                ", city='" + city + '\'' +
                ", allocationInPercentage='" + allocationInPercentage + '\'' +
                '}';
    }
}
