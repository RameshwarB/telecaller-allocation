package com.lentra;

import com.lentra.readcsvfiles.hfl.ReadingFile;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class HflTelecallerApplication {
    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(HflTelecallerApplication.class, args);
        System.out.println("Hfl application started......");
        ReadingFile readingFile = ctx.getBean(ReadingFile.class);
        readingFile.readFile();
    }
}
