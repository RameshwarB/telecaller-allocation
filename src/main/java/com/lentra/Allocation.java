package com.lentra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Allocation {

    public static void main(String[] args) {
        Allocation test = new Allocation();
        Map<String, List<String>> accList = new HashMap<String, List<String>>();

        List<String> kolkataList = new ArrayList<String>();
        kolkataList.add("KolAcct1");
        kolkataList.add("KolAcct2");
        kolkataList.add("KolAcct3");
        kolkataList.add("KolAcct4");
        kolkataList.add("KolAcct5");
        kolkataList.add("KolAcct6");
        accList.put("Kolkata", kolkataList);

        List<String> raipurList = new ArrayList<String>();
        raipurList.add("RaiAcct1");
        raipurList.add("RaiAcct2");
        raipurList.add("RaiAcct3");
        accList.put("Raipur", raipurList);

        Map<String, Map<String, Double>> telecallerMap = test.getTelecallerMap();

        for (Entry<String, List<String>> entry : accList.entrySet())  {

            String city = entry.getKey();
            List<String> cityAccList = entry.getValue();
            Iterator<String> accIterator = cityAccList.iterator();

            if (telecallerMap.containsKey(city)) {

                Map<String, Double> teleCallerList = telecallerMap.get(city);
                if (teleCallerList.size() == 1) {

                    Iterator<String> teleCallerIterator = teleCallerList.keySet().iterator();
                    String telecallerName = teleCallerIterator.next();

                    System.out.println("Only 1 telecaller for " + city);
                    // Allocate all accounts to this telecaller
                    while (accIterator.hasNext()) {
                        System.out.println("Allcoating account " + accIterator.next() + " to telecaller " + telecallerName);
                    }
                } else {

                    // There is more than 1 telecaller. Do RoundRobin allocation
                    while (accIterator.hasNext()) {

                        Iterator<String> teleCallerIterator = teleCallerList.keySet().iterator();
                        while (teleCallerIterator.hasNext() && accIterator.hasNext()) {

                            String telecallerName = teleCallerIterator.next();
                            System.out.println("Allcoating account " + accIterator.next() + " to telecaller " + telecallerName);
                        }
                    }
                }
            }
        }
    }

    public void teleCallerAllocation(final List<Integer> accList, final Integer reqList, final int telecallerNum) {
        System.out.println(telecallerNum + "::" + reqList);

    }

    private static Map<String, Map<String, Double>> getTelecallerMap() {

        Map<String, Map<String, Double>> telecallerMap = new HashMap<String, Map<String, Double>>();
        try {
            Map<String, Double> kolkataData = new HashMap<String, Double>();
            kolkataData.put("KA", 25.0);
            kolkataData.put("KB", 25.0);
            kolkataData.put("KC", 25.0);
            kolkataData.put("KD", 25.0);

            telecallerMap.put("Kolkata", kolkataData);

            Map<String, Double> raipurData = new HashMap<String, Double>();
            raipurData.put("RA", 100.0);
            telecallerMap.put("Raipur", raipurData);
        } catch (Exception e) {

            e.printStackTrace();
        }

        return telecallerMap;
    }
}

