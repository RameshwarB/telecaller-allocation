package com.lentra;

import com.lentra.readcsvfiles.fsl.FslCsvRead;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class FslTelecallerApplication {
    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(FslTelecallerApplication.class, args);
        System.out.println("Fsl application started.....");
        FslCsvRead readingFile = ctx.getBean(FslCsvRead.class);
        readingFile.readCsvFile();
    }
}
