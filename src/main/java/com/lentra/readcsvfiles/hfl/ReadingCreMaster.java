package com.lentra.readcsvfiles.hfl;

import com.lentra.domain.hfl.CreMaster;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ReadingCreMaster {

    @Value("${file.hfl.creMaster.path}")
    private static String creMasterFilePath;
    public static Map<String,List<CreMaster>> creMasterMapping = getCreMasterMapping();

    private static Map<String,List<CreMaster>> getCreMasterMapping() {
        Map<String, String> mapping = new HashMap<>();
        /*mapping.put("Region", "region");
        mapping.put("State", "state");
        mapping.put("TL NAME AND NUMBER", "tlNameAndNumber");*/
        mapping.put("CRE NAME AND NUMBER", "creNameAndNumber");
        mapping.put("CRE Working Locations/ Area", "creArea");
        mapping.put("City", "city");
        mapping.put("Allocation%", "allocationInPercentage");

        HeaderColumnNameTranslateMappingStrategy<CreMaster> strategy = new HeaderColumnNameTranslateMappingStrategy<>();
        strategy.setType(CreMaster.class);
        strategy.setColumnMapping(mapping);
        List<CreMaster> creMasterDomain;
        Map<String,List<CreMaster>> cityMaster = new HashMap<>();
        try {
            CsvToBeanBuilder<CreMaster> csvToBeanBuilder = new CsvToBeanBuilder<>(new FileReader("D:\\Ram\\MB2.0 Projects Doc\\Task\\StandAloneApplication TCFL\\TCHFL\\Inputs file\\CRE Master.csv"));
            creMasterDomain = csvToBeanBuilder.withMappingStrategy(strategy).build().parse();
            for (CreMaster creMaster : creMasterDomain) {
                cityMaster.computeIfAbsent(creMaster.getCity(),k -> new ArrayList<>()).add(creMaster);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return cityMaster;
    }
}
