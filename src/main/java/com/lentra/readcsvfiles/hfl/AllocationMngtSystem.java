package com.lentra.readcsvfiles.hfl;

import com.lentra.domain.hfl.AccountDomain;
import com.lentra.domain.hfl.CreMaster;
import com.opencsv.CSVWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Component
public class AllocationMngtSystem {

    @Value("${file.hfl.outputFile.path}")
    private String outputFile;

    public void callAllocationSystem(Map<String, List<AccountDomain>> accMap) {
        Map<String, List<CreMaster>> cityMaster = ReadingCreMaster.creMasterMapping;
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(outputFile));
            // adding header to csv
            String[] header = {"account number", "allocation status", "bucket status", "telecaller details"};
            writer.writeNext(header);
            for (Map.Entry<String, List<AccountDomain>> entry : accMap.entrySet()) {
                String city = entry.getKey();
                List<AccountDomain> cityAccList = entry.getValue();
                Iterator<AccountDomain> accIterator = cityAccList.iterator();
                if (cityMaster.containsKey(city)) {

                    List<CreMaster> teleCallerList = cityMaster.get(city);
                    if (teleCallerList.size() == 1) {
                        CreMaster telecallerList = teleCallerList.iterator().next();
                        String creNameAndNumber = telecallerList.getCreNameAndNumber();
                        System.out.println("Only 1 telecaller for the :: " + city);
                        System.out.println("There are " + cityAccList.size() + " Accounts");
                        // Allocate all accounts to this telecaller
                        while (accIterator.hasNext()) {
                            AccountDomain accObject = accIterator.next();
                            System.out.println("Allcoating account " + accObject.getAccountNo() + " to telecaller " + creNameAndNumber);
                            String[] outputData = {accObject.getAccountNo(), accObject.getAllocationFlag(), accObject.getBucketingFlag(),
                                    accObject.getAllocationFlag().equalsIgnoreCase("Qconnect Field") ? creNameAndNumber: ""};
                            writer.writeNext(outputData);
                        }
                    } else {
                        // There is more than 1 telecaller. Do RoundRobin allocation
                        System.out.println("There are ::" + teleCallerList.size() + " telecaller for the: " + city);
                        System.out.println("There are " + cityAccList.size() + " Accounts");
                        while (accIterator.hasNext()) {
                            Iterator<CreMaster> teleCallerIterator = teleCallerList.iterator();
                            while (teleCallerIterator.hasNext() && accIterator.hasNext()) {
                                AccountDomain accObject = accIterator.next();
                                CreMaster telecallerList = teleCallerIterator.next();
                                String creNameAndNumber = telecallerList.getCreNameAndNumber();
                                System.out.println("Allcoating account " + accObject.getAccountNo() + " to telecaller " + creNameAndNumber);
                                String[] outputData = {accObject.getAccountNo(), accObject.getAllocationFlag(),
                                        accObject.getBucketingFlag(), accObject.getAllocationFlag().equalsIgnoreCase("Qconnect Field") ? creNameAndNumber: ""};
                                writer.writeNext(outputData);
                            }
                        }
                    }
                } else {
                    System.out.println(">>>>>>>>>>>Telecaller not found>>>>>>>>");
                    while (accIterator.hasNext()) {
                        AccountDomain accObject = accIterator.next();
                        System.out.println("Allcoating account: " + accObject.getAccountNo() + " Telecaller Not found..");
                        String[] outputData = {accObject.getAccountNo(), accObject.getAllocationFlag(),
                                accObject.getBucketingFlag()};
                        writer.writeNext(outputData);
                    }
                }
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
