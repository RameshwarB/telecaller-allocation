package com.lentra.readcsvfiles.hfl;

import com.lentra.breprocess.BreCalculation;
import com.lentra.domain.hfl.AccountDomain;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ReadingFile {

    @Value("${file.hfl.bnc.path}")
    private String bncFilePath;
    @Value("${file.hfl.hml.path}")
    private String hmlFilePath;
    @Value("${file.hfl.paymentfile.path}")
    private String paymentFilePath;
    @Value("${file.hfl.referral.path}")
    private String referralFilePath;
    @Value("${file.hfl.lbr.path}")
    private String lbrFilePath;
    public static Map<String, String> accMapping = getAccMapping();
    @Autowired
    BreCalculation breCalculation;

    private static Map<String, String> getAccMapping() {
        Map<String, String> mapping = new HashMap<>();
        mapping.put("Account No", "accountNo");
        mapping.put("Bounce Status", "bounceStatus");
        mapping.put("Segment", "segment");
        mapping.put("Band", "band");
        mapping.put("Referral Y/N", "referralYesNo");
        mapping.put("Payment", "payment");
        mapping.put("Interest", "interest");
        mapping.put("Charges", "charges");
        mapping.put("Irregularity (OD)", "irregularity_OD");
        mapping.put("Additional Interest (Overdue Charges)", "additionalInterest_OC");
        mapping.put("DPD", "dpd");
        mapping.put("NPA/WROFF", "npa_wroff");
        mapping.put("Final bucketxx", "finalBucketxx");
        mapping.put("EMI", "emi");
        mapping.put("System", "system");
        mapping.put("Irre/Emi", "irreEmi");
        mapping.put("REVISED_LOB", "revised_Lob");
        mapping.put("Subproduct Description", "subProductDescription");
        mapping.put("CIF No", "cifNo");
        mapping.put("AHF/NON AHF", "ahf_NonAhf");
        mapping.put("Status", "status");
        mapping.put("Branch Code", "branchCode");
        mapping.put("Branch", "branch");
        mapping.put("Cluster", "cluster");
        mapping.put("Cluster2", "cluster2");
        mapping.put("Zone", "zone");
        mapping.put("Region", "region");
        mapping.put("Contract Start Date", "contractStartDate");
        mapping.put("Contract End Date", "contractEndDate");
        mapping.put("Allocation Flag", "allocationFlag");
        mapping.put("CRE Code", "creCode");
        return mapping;
    }

    public void readFile() {
        System.out.println("Inside reading file===========>>");
        // HeaderColumnNameTranslateMappingStrategy
        HeaderColumnNameTranslateMappingStrategy<AccountDomain> strategy = new HeaderColumnNameTranslateMappingStrategy<>();
        strategy.setType(AccountDomain.class);
        strategy.setColumnMapping(accMapping);
        List<AccountDomain> bncDomains;
        List<AccountDomain> hmlDomains;
        List<AccountDomain> paymentDomains;
        List<AccountDomain> referralDomains;
        List<AccountDomain> lbrDomains;
        List<AccountDomain> finalDomain;

        try {
            System.out.println("Reading bncFile===============>");
            CsvToBeanBuilder<AccountDomain> csvToBeanBuilder = new CsvToBeanBuilder<>(new FileReader(bncFilePath));
            System.out.println("Reading hmlFile===============>");
            CsvToBeanBuilder<AccountDomain> csvToBeanBuilder2 = new CsvToBeanBuilder<>(new FileReader(hmlFilePath));
            System.out.println("Reading paymentFile===============>");
            CsvToBeanBuilder<AccountDomain> csvToBeanBuilder3 = new CsvToBeanBuilder<>(new FileReader(paymentFilePath));
            System.out.println("Reading referralFile===============>");
            CsvToBeanBuilder<AccountDomain> csvToBeanBuilder4 = new CsvToBeanBuilder<>(new FileReader(referralFilePath));
            System.out.println("Reading lbrFile===============>");
            CsvToBeanBuilder<AccountDomain> csvToBeanBuilder5 = new CsvToBeanBuilder<>(new FileReader(lbrFilePath));
            System.out.println("All files are completed ===============>");
            System.out.println("parsing LBR Domain ===============>");
            lbrDomains = csvToBeanBuilder5.withMappingStrategy(strategy).build().parse();
            System.out.println("parsing Bnc Domain ===============>");
            bncDomains = csvToBeanBuilder.withMappingStrategy(strategy).build().parse();
            System.out.println("parsing HML Domain ===============>");
            hmlDomains = csvToBeanBuilder2.withMappingStrategy(strategy).build().parse();
            System.out.println("parsing Payment Domain ===============>");
            paymentDomains = csvToBeanBuilder3.withMappingStrategy(strategy).build().parse();
            System.out.println("parsing Referral Domain ===============>");
            referralDomains = csvToBeanBuilder4.withMappingStrategy(strategy).build().parse();

            for (int i = 0; i < referralDomains.size(); i++) {
                System.out.println("Inside the refererral >>>>>>:: " + i);
                splitReferralDate(referralDomains.get(i));
                bncDomains.add(referralDomains.get(i));
            }
            finalDomain = toMergeDomain(bncDomains);
            System.out.println("merged referral to bnc object=============>");
            finalDomain.addAll(hmlDomains);
            finalDomain = toMergeDomain(finalDomain);
            System.out.println("merged hml obj to finalAccObj object============>>");
            finalDomain.addAll(paymentDomains);
            finalDomain = toMergeDomain(finalDomain);
            System.out.println("merged paymentFile obj to finalAccObj object===========>>");
            finalDomain.addAll(lbrDomains);
            finalDomain = toMergeDomain(finalDomain);
            System.out.println("merged lbr obj to finalAccObj object===============>>");
            System.out.println("All Object are mapped in single List of object =============>>");
            breCalculation.breRequestProcess(finalDomain);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void splitReferralDate(AccountDomain referralJson) {
        String referralString = referralJson.getReferralYesNo();
        String[] referralArray = referralString.split("-");
        referralJson.setReferralYesNo(referralArray[0].trim());
        referralJson.setReferralDate(referralArray[1].trim());
    }

    private List<AccountDomain> toMergeDomain(List<AccountDomain> bncDomain) {
        List<AccountDomain> finalAccObj = new ArrayList<>();
        for (int i = 0; i < bncDomain.size(); i++) {
            //System.out.println("Mereging domain started ...: " + i++);
            try {
                AccountDomain outerObject = bncDomain.get(i);
                AccountDomain finalInnerObj = null;
                for (int j = i + 1; j < bncDomain.size(); j++) {
                    AccountDomain innerObject = bncDomain.get(j);
                    if (outerObject != null && innerObject != null) {
                        if (StringUtils.isNotBlank(outerObject.getAccountNo())) {
                            if (outerObject.getAccountNo().equals(innerObject.getAccountNo())) {
                                finalInnerObj = new AccountDomain();
                                finalInnerObj.setAccountNo(innerObject.getAccountNo());
                                finalInnerObj.setSegment(StringUtils.isNotBlank(innerObject.getSegment()) ? innerObject.getSegment() : outerObject.getSegment());
                                finalInnerObj.setBand(StringUtils.isNotBlank(innerObject.getBand()) ? innerObject.getBand() : outerObject.getBand());
                                finalInnerObj.setBounceStatus(StringUtils.isNotBlank(innerObject.getBounceStatus()) ? innerObject.getBounceStatus() : outerObject.getBounceStatus());
                                finalInnerObj.setReferralYesNo(StringUtils.isNotBlank(innerObject.getReferralYesNo()) ? innerObject.getReferralYesNo() : outerObject.getReferralYesNo());
                                finalInnerObj.setReferralDate(StringUtils.isNotBlank(innerObject.getReferralDate()) ? innerObject.getReferralDate() : outerObject.getReferralDate());
                                finalInnerObj.setPayment(StringUtils.isNotBlank(innerObject.getPayment()) ? innerObject.getPayment() : outerObject.getPayment());
                                finalInnerObj.setInterest(StringUtils.isNotBlank(innerObject.getInterest()) ? innerObject.getInterest() : outerObject.getInterest());
                                finalInnerObj.setCharges(StringUtils.isNotBlank(innerObject.getCharges()) ? innerObject.getCharges() : outerObject.getCharges());
                                finalInnerObj.setIrregularity_OD(StringUtils.isNotBlank(innerObject.getIrregularity_OD()) ? innerObject.getIrregularity_OD() : outerObject.getIrregularity_OD());
                                finalInnerObj.setAdditionalInterest_OC(StringUtils.isNotBlank(innerObject.getAdditionalInterest_OC()) ? innerObject.getAdditionalInterest_OC() : outerObject.getAdditionalInterest_OC());
                                finalInnerObj.setDpd(StringUtils.isNotBlank(innerObject.getDpd()) ? innerObject.getDpd() : outerObject.getDpd());
                                finalInnerObj.setNpa_wroff(StringUtils.isNotBlank(innerObject.getNpa_wroff()) ? innerObject.getNpa_wroff() : outerObject.getNpa_wroff());
                                finalInnerObj.setFinalBucketxx(StringUtils.isNotBlank(innerObject.getFinalBucketxx()) ? innerObject.getFinalBucketxx() : outerObject.getFinalBucketxx());
                                finalInnerObj.setEmi(StringUtils.isNotBlank(innerObject.getEmi()) ? innerObject.getEmi() : outerObject.getEmi());
                                finalInnerObj.setSystem(StringUtils.isNotBlank(innerObject.getSystem()) ? innerObject.getSystem() : outerObject.getSystem());
                                finalInnerObj.setIrreEmi(StringUtils.isNotBlank(innerObject.getIrreEmi()) ? innerObject.getIrreEmi() : outerObject.getIrreEmi());
                                finalInnerObj.setRevised_Lob(StringUtils.isNotBlank(innerObject.getRevised_Lob()) ? innerObject.getRevised_Lob() : outerObject.getRevised_Lob());
                                finalInnerObj.setSubProductDescription(StringUtils.isNotBlank(innerObject.getSubProductDescription()) ? innerObject.getSubProductDescription() : outerObject.getSubProductDescription());
                                finalInnerObj.setCifNo(StringUtils.isNotBlank(innerObject.getCifNo()) ? innerObject.getCifNo() : outerObject.getCifNo());
                                finalInnerObj.setAhf_NonAhf(StringUtils.isNotBlank(innerObject.getAhf_NonAhf()) ? innerObject.getAhf_NonAhf() : outerObject.getAhf_NonAhf());
                                finalInnerObj.setStatus(StringUtils.isNotBlank(innerObject.getStatus()) ? innerObject.getStatus() : outerObject.getStatus());
                                finalInnerObj.setBranchCode(StringUtils.isNotBlank(innerObject.getBranchCode()) ? innerObject.getBranchCode() : outerObject.getBranchCode());
                                finalInnerObj.setBranch(StringUtils.isNotBlank(innerObject.getBranch()) ? innerObject.getBranch() : outerObject.getBranch());
                                finalInnerObj.setCluster(StringUtils.isNotBlank(innerObject.getCluster()) ? innerObject.getCluster() : outerObject.getCluster());
                                finalInnerObj.setCluster2(StringUtils.isNotBlank(innerObject.getCluster2()) ? innerObject.getCluster2() : outerObject.getCluster2());
                                finalInnerObj.setZone(StringUtils.isNotBlank(innerObject.getZone()) ? innerObject.getZone() : outerObject.getZone());
                                finalInnerObj.setRegion(StringUtils.isNotBlank(innerObject.getRegion()) ? innerObject.getRegion() : outerObject.getRegion());
                                finalInnerObj.setContractStartDate(StringUtils.isNotBlank(innerObject.getContractStartDate()) ? innerObject.getContractStartDate() : outerObject.getContractStartDate());
                                finalInnerObj.setContractEndDate(StringUtils.isNotBlank(innerObject.getContractEndDate()) ? innerObject.getContractEndDate() : outerObject.getContractEndDate());
                                finalInnerObj.setAllocationFlag(StringUtils.isNotBlank(innerObject.getAllocationFlag()) ? innerObject.getAllocationFlag() : outerObject.getAllocationFlag());
                                finalInnerObj.setCreCode(StringUtils.isNotBlank(innerObject.getCreCode()) ? innerObject.getCreCode() : outerObject.getCreCode());
                            }
                        }
                    }
                }
                if (finalInnerObj != null) {
                    finalAccObj.add(finalInnerObj);
                } else {
                    if (StringUtils.isNotBlank(outerObject.getAccountNo()) && !finalAccObj.toString().contains(outerObject.getAccountNo())) {
                        finalAccObj.add(outerObject);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return finalAccObj;
    }

}
