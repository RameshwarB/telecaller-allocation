package com.lentra.readcsvfiles.fsl;

import com.lentra.domain.hfl.CreMaster;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

@Component
public class ReadingAgencyMaster {

    private String dataFilePath;

    public static Map<String, String> agencyMasterMapping = getAgencyMasterMapping();

    private static Map<String, String> getAgencyMasterMapping() {
        Map<String, String> mapping = new HashMap<>();
        mapping.put("AGENCY CODE", "agencyCode");
        mapping.put("AGENCY NAME", "agencyName");
        mapping.put("BRANCH CODES SERVED", "branchCodeServed");
        mapping.put("PINCODES SERVED", "pinCodeServed");
        mapping.put("PRODUCTS SERVED", "productsServed");
        mapping.put("BOM BUCKETS SERVED", "bomBucketsServed");
        mapping.put("VERTICAL", "vertical");
        mapping.put("AGENCY PERFORMANCE (PRODUCT & BRANCH SPECIFIC)", "agencyPerformance");
        mapping.put("AGENCY CAPACITY - MANPOWER WISE", "agencyCapacity");
        mapping.put("AGENCY STATUS", "agencyStatus");
        /*mapping.put("CM", "cm");
        mapping.put("CCM", "ccm");
        mapping.put("RCM", "rcm");
        mapping.put("NCM", "ncm");*/

        return mapping;
    }

    public void readAgency() {

        HeaderColumnNameTranslateMappingStrategy<ReadingAgencyMaster> strategy = new HeaderColumnNameTranslateMappingStrategy<>();
        strategy.setType(ReadingAgencyMaster.class);
        strategy.setColumnMapping(agencyMasterMapping);
        List<ReadingAgencyMaster> readingAgencyMaster;
        try {

            CsvToBeanBuilder<ReadingAgencyMaster> csvToBeanBuilder = new CsvToBeanBuilder<>(new FileReader("D:\\Ram\\MB2.0 Projects Doc\\Task\\StandAloneApplication TCFL\\TCFSL\\input files\\AGENCY MASTER.csv"));
            readingAgencyMaster = csvToBeanBuilder.withMappingStrategy(strategy).build().parse();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
