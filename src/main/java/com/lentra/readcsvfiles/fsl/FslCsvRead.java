package com.lentra.readcsvfiles.fsl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lentra.domain.fsl.DataDomian;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class FslCsvRead {

    @Autowired
    ReadingAgencyMaster readAgency;

    @Value("${file.fsl.data.path}")
    private String dataFilePath;

    public static Map<String, String> accMapping = getAccMapping();

    private static Map<String, String> getAccMapping() {
        Map<String, String> mapping = new HashMap<>();
        mapping.put("LOAN NO","loanNo");
        mapping.put("BOM ALLOCATION","bomAllocation");
        mapping.put("DO NOT ALLOCATE FLAG","dontAllocateFlag");
        mapping.put("SETTLEMENT FLAG","settlementFlag");
        mapping.put("NON X IN L3","nonXInL3");
        mapping.put("RF IN L3","rfInL3");
        mapping.put("AVG RESOLUTION DATE L3","avgReslutionDateL3");
        mapping.put("CYCLE DAY","cycleDay");
        mapping.put("RESOLVED BY HOLD IN LAST MONTH","resolvedByHoldInLastMonth");
        mapping.put("RESOLUTION DAY - ALLOCATION DAY AVG OF L3","resolutionAllocationDayAvgOfL3");
        mapping.put("RESOLVED BY HOLD IN L3","resolvedByHoldInL3");
        mapping.put("REPRESENT CLEARANCE IN L3","representClearanceInL3");
        mapping.put("BIU SELF CURE DECILE","biuSelfCureDecile");
        mapping.put("LINK BASED PAYMENT","linkBasedPayment");
        mapping.put("ALLOCATED TO FIELD IN ALL L3","AllocatedToFieldInAllL3");
        mapping.put("HOLD(RF) TO CC/FIELD IN ALL L3","holdRfToCcOrFieldInAllL3");
        mapping.put("BIU REP SEGMENT 0 - 10","biuRepSegment0_10");
        mapping.put("HOLD LOCATION - BIU REPRESENTATION (LOW - CLEARED/PENDING/NA)","HoldLocBiuRepreLow_Cleared");
        mapping.put("HOLD LOCATION - BIU REPRESENTATION (LOW - BOUNCED)","HoldLocBiuRepreLow_Bounced");
        mapping.put("BIU REPRESENTATION (LOW - CLEARED/PENDING/NA)","biuRepreLow_Cleared");
        mapping.put("BIU REPRESENTATION (LOW - BOUNCED)","biuRepreLow_Bounced");
        mapping.put("NON CC PRODUCTS - LAP_CV_NC_UBER","nonCCProd_Lap_Cv_Nc_Uber");
        mapping.put("CC RES LAST MONTH","ccResLastMonth");
        mapping.put("CC RESOLVED IN L3","ccResolvedInL3");
        mapping.put("DIGITAL PAYMENTS","digitalPayments");
        mapping.put("BIU RISK SEGMENT","biuRiskSegment");
        mapping.put("MOB","mob");
        mapping.put("BRANCH CODE","branchCode");
        mapping.put("CITY NAME","cityName");
        mapping.put("PINCODE","pinCode");
        mapping.put("BROAD FLAG","broadFlag");
        mapping.put("AGENCY FLAG","agencyFlag");
        mapping.put("PRODUCT","product");
        mapping.put("BOM BKT_1","bomBkt1");
        mapping.put("DISPOSITION CODE","disPositionCode");
        return mapping;
    }

    public void readCsvFile(){

        readAgency.readAgency();
        System.out.println("Inside reading file===========>>");
        // HeaderColumnNameTranslateMappingStrategy
        HeaderColumnNameTranslateMappingStrategy<DataDomian> strategy = new HeaderColumnNameTranslateMappingStrategy<>();
        strategy.setType(DataDomian.class);
        strategy.setColumnMapping(accMapping);
        List<DataDomian> dataDomains;
        try {
            ObjectMapper mapper = new ObjectMapper();
            CsvToBeanBuilder<DataDomian> csvToBeanBuilder = new CsvToBeanBuilder<>(new FileReader(dataFilePath));
            dataDomains = csvToBeanBuilder.withMappingStrategy(strategy).build().parse();
            // below condition is handled in bre
            /*dataDomains = dataDomains.parallelStream().map(dataRow -> {
                 if (dataRow.getDisPositionCode().equalsIgnoreCase("CCREFEROUT") && dataRow.getBroadFlag().equalsIgnoreCase("FIELD")) {
                     return dataRow;
                 } else {
                     return null;
                 }
            } ).filter(Objects::nonNull).collect(Collectors.toList());*/
            JSONArray dataJson = new JSONArray(mapper.writeValueAsString(dataDomains));
            System.out.println("dataDomain::" + dataJson);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
