package com.lentra.breprocess;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BreConnector {

    @Value("${bre.hfl.url}")
    private String url;
    @Value("${bre.hfl.institutionId}")
    private String institutionId;
    @Value("${bre.hfl.userId}")
    private String userId;
    @Value("${bre.hfl.password}")
    private String password;
    @Value("${bre.hfl.connectTimeout}")
    private int connectTimeout;
    @Value("${bre.hfl.socketTimeout}")
    private int socketTimeout;


    public String getBreResponse(JSONObject breRequest) {
        System.out.println("=========>Inside the Bre call==========>");
        String response = null;
        try {
            Client client = Client.create();
            client.setConnectTimeout(connectTimeout);
            client.setReadTimeout(socketTimeout);
            WebResource webResource = client.resource(url);
            response = webResource.path("CalculateScoreSync")
                    .queryParam("INSTITUTION_ID", institutionId)
                    .queryParam("USER_ID", userId)
                    .queryParam("PASSWORD", password)
                    .entity(breRequest.toString(), "application/json").post(String.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
       return response;
    }
}
