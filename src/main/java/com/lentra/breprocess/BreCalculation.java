package com.lentra.breprocess;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lentra.domain.hfl.*;
import com.lentra.readcsvfiles.hfl.AllocationMngtSystem;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class BreCalculation {

    @Autowired
    BreConnector breConnector;
    @Autowired
    AllocationMngtSystem allocationMngtSystem;
    @Value("${bre.hfl.institutionId}")
    private String institutionId;

    public void breRequestProcess(List<AccountDomain> finalAccObj) {
        Map<String, List<AccountDomain>> cityAllocationMap = new HashMap<>();
        int count = 0;
        for (AccountDomain accObj : finalAccObj) {
            try {
                String breResponse = getBreRequest(accObj);
                JSONObject breRespJson = new JSONObject(breResponse);
                JSONObject derivedFields = breRespJson.optJSONObject("DERIVED_FIELDS");
                accObj.setAllocationFlag(derivedFields.optString("CUSTOM_FIELDS$ALLOCATION_FLAG"));
                accObj.setBucketingFlag(derivedFields.optString("CUSTOM_FIELDS$BUCKET_X_FLAG"));
                cityAllocationMap.computeIfAbsent(accObj.getBranch(), k -> new ArrayList<>()).add(accObj);
                System.out.println("========RequestCount:: " + count++);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       // System.out.println("Final Allocation City list::" + cityAllocationMap.toString());
        allocationMngtSystem.callAllocationSystem(cityAllocationMap);
    }

    private String getBreRequest(AccountDomain loanAcc) {
        JSONObject breRequest = createBreRequest(loanAcc);
        System.out.println(">>>>>>>>>bre request created<<<<<<<<<<<<<<<<");
        String breResponse = breConnector.getBreResponse(breRequest);
        System.out.println("Bre Response ::" + breResponse);
        return breResponse;
    }

    private JSONObject createBreRequest(AccountDomain loanAcc) {
        JSONObject breRequest = null;
        try {
            HflCollectRequestDomain hflCollectRequestDomain = new HflCollectRequestDomain();
            BreRequestDomain breRequestDomain = new BreRequestDomain();
            HflRequest hflRequest = new HflRequest();
            Header header = new Header();

            header.setApplicationId(loanAcc.getAccountNo());
            header.setCustomerId(loanAcc.getAccountNo());
            header.setAppType("COLLECTION");
            header.setInstitutionId(institutionId);
            header.setRequestTime(String.valueOf(new Date()));
            Application application = new Application();
            application.setsLoanType("CONSTRUCTION FINANCE-TCHFL");
            hflRequest.setAccountDomain(loanAcc);
            hflRequest.setApplication(application);
            hflCollectRequestDomain.setHflRequest(hflRequest);
            breRequestDomain.setHeader(header);
            breRequestDomain.setHflCollectRequestDomain(hflCollectRequestDomain);
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            breRequest = new JSONObject(gson.toJson(breRequestDomain));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return breRequest;
    }
}
